//
//  main.cpp
//  laba2bsemestr3
//
//  Created by Николай on 08.10.14.
//  Copyright (c) 2014 Николай. All rights reserved.
//


#include <iostream>
#include "1.h"
#include <iostream>
const char *msgs[] = { "0.Konets Raboty", "1.Vivod na Ekran", "2.Zapolneniye", "3.Poshchitat' summu",
    "4.Zapolnenie s zadannym kolichestvom cifr posle zapyatoi", "5.Obnovit' vyborku", "6.Vichislit' srednee",
    "7.Vivod cherez peregr. operator" ,"9. dopolnenie do 1","10. diapazon","11.sluchainiy","12.Priravnivaniye"};
const int NMsgs = sizeof(msgs) / sizeof(msgs[0]);
Figures::Massiv nabor;
int vivod(){
    std::cout << nabor << std::endl;
    return 0;
}
int diapazon(){
    std::cout << "Vvedite min,zatem max" << std::endl;
    int min, max;
    std::cin >> min >> max;
    std::cout << (nabor(min, max)) << std::endl;
    return 0;
}
int Maxa4kalavivodsluchainogo(){
    int x = 0;
    std::cout << "Vvedite nomer elementa(numeraciya s nulya)" << std::endl;
    std::cin >> x;
    try{
        std::cout << nabor[x] << std::endl;
    }
    catch (const char*){
        std::cout << "Illegal index" << std::endl;
    }
    return 0;
}
int Priravnivaniye(){
    std::cout << "Vvedite index,zatem znacheniye" << std::endl;
    int x; int y;
    std::cin >> x >> y;
    if (y < 0 || y>1)
        std::cout << "Znacheniye dolzhno byt >=0 i <=1";
    try{
        nabor[x] = y;
    }
    catch (const char*){
        std::cout << "Illegal Index" << std::endl;
    }
    std::cout << nabor << std::endl;
    return 0;
}
int zapolneniye(){
    int x;
    std::cout << "Vvedite kolichestvo elementov(>=0)" << std::endl;
    std::cin >> x;
    try{
        nabor.zapolnenie(x);
    }
    catch (std::exception){
        std::cout << "Nepravilno vvedeno kolvo" << std::endl;
    }
    std::cout << nabor << std::endl;
    return 0;
}
int vivodcherezoperator(){
    std::cout << nabor;
    return 0;
}
int NazranovskiyPovorot(){
    std::cout << (~nabor) << std::endl;
    return 0;
}
int zapolneniesdlinoi(){
    int x;
    std::cout << "Vvedite kolichestvo cifr posle zapyatoi(>=1)" << std::endl;
    std::cin >> x;
    int quant;
    std::cout << "Vvedite kolvo elementov(>=0)" << std::endl;
    std::cin >> quant;
    try{
        nabor.zapolnenie(quant, x);
    }
    catch (std::exception){
        std::cout << "Neverno vvedeni parametri" << std::endl;
    }
    nabor.vivod();
    return 0;
}
int podshchetsummy(){
    std::cout << nabor.podshchet() << std::endl;
    return 0;
}
int obnovlenie(){
    std::cout << nabor << std::endl;
    return nabor.obnovlenie();
}
int srednee(){
    std::cout << "Srednee ravno" << nabor.srednee() << std::endl;
    return 0;
}
int(*funcs[])() = { NULL, vivod, zapolneniye, podshchetsummy, zapolneniesdlinoi, obnovlenie, srednee,
    vivodcherezoperator, NazranovskiyPovorot, diapazon, Maxa4kalavivodsluchainogo, Priravnivaniye };
int dialog(const char *msgs[], int N){
    char * errmsg = " ";
    int rc, i, n;
    do{
        puts(errmsg);
        errmsg = "Oshibka.Povtorite,pozhaluista";
        for (i = 0; i<N; ++i)
            puts(msgs[i]);
        puts("Sdelayte vybor->");
        std::cin >> rc;
        n = 1;//slfdkvm
        if (n == 0)
            rc = 0;
    } while (rc<0 || rc >= N);
    return rc;
}
int main(){
    int rc = 0;
    while (rc = dialog(msgs, NMsgs))
        if (funcs[rc]())
            break;
    printf("Eto vse.Dosvidaniya\n");
    return 0;
}