//
//  1.cpp
//  laba2bsemestr3
//
//  Created by Николай on 06.10.14.
//  Copyright (c) 2014 Николай. All rights reserved.
//

#include "1.h"
#include <cmath>
#include <cstdio>
#include <iostream>
#include <time.h>

using namespace Figures;

//Circle::Circle() :xy(0, 0), aobr(0){}
//Circle::~Circle(){}
int Massiv::obnovlenie(){
    for (int i = 0; i < dlina; i++){
        int x = rand();
        mas[i] = x;// / RAND_MAX;
    }
    return 0;
}
int Massiv::zapolnenie(int kolvo){
    if (kolvo < 0){
        throw ("illegal parameter: dlina");
    }
    dlina = kolvo;
    mas = new int[kolvo];
    for (int i = 0; i < dlina; i++){
        int x = rand();
        mas[i] = x;// / RAND_MAX;
    }
    return 0;
}
int Massiv::srednee()const{
    int sum = 0;
    if (dlina == 0)
        return 0;
    for (int i = 0; i < dlina; i++)
        sum += mas[i];
    return (sum / dlina);
}
int Massiv::dlinaq()const{
    return dlina;
}
Massiv & Massiv::operator ++(){
    int* q = mas;
    mas = new int[dlina + 1];
    for (int i = 0; i < dlina; i++){
        mas[i] = q[i];
    }
    delete(q);
    int x = rand();
    mas[dlina] = x;// / RAND_MAX;
    dlina++;
    return *this;
}
Massiv Massiv::operator ~(){
    Massiv q;
    q.dlina = dlina;
    q.mas = new int[dlina];
    int x = 1;
    for (int i = 0; i < dlina; i++){
        q.mas[i] = x - mas[i];
    }
    return q;
}
Massiv Massiv::operator()(int min, int max){
    int q = 0;
    int qmax = 0;
    Massiv massiv;
    for (int i = 0; i < (*this).dlina; i++){
        if (min < (*this).mas[i] && (*this).mas[i] < max){
            qmax++;
        }
    }
    if (qmax == 0)
        return massiv;
    massiv.dlina = qmax;
    massiv.mas = new int[qmax];
    for (int i = 0; i < (*this).dlina; i++){
        if (min < (*this).mas[i] && (*this).mas[i] < max){
            massiv.mas[q] = mas[i];
            q++;
        }
    }
    return massiv;
}
int &Massiv::operator[](int x){
    if (x<0 || x>=dlina)
        throw"Illegal index";
    
    return mas[x];
}
int Massiv::operator[](int x)const{
    if (x<0 || x>=dlina)
        throw ("Illegal index");
    return ((*this).mas[x]);
}
Massiv& Massiv::operator =(const Massiv&x){
    dlina = x.dlina;
    mas = new int[dlina];
    for (int i = 0; i < dlina; i++){
        mas[i] = x.mas[i];
    }
    return *this;
}