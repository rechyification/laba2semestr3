//
//  1.h
//  laba2bsemestr3
//
//  Created by Николай on 08.10.14.
//  Copyright (c) 2014 Николай. All rights reserved.
//

#ifndef __laba2semestr3_____
#define __laba2semestr3_____

#include <stdio.h>
#include <cstdlib>
#include <iostream>
namespace Figures
{
    class Massiv{
    private:
        int dlina;
        int* mas;
    public:
        Massiv() :dlina(0), mas(NULL){};
        Massiv(int kolvo, int s){
            if (kolvo < 0)
                throw ("lsdjls");
            if (kolvo == 0){
                mas = NULL;
                dlina = kolvo;
            }
            mas = new int[kolvo];
            srand(s);
            for (int i = 0; i < kolvo;i++){
                int x = rand();
                mas[i] = x;///RAND_MAX;
            }
            dlina = kolvo;
        }
        Massiv(int a) :dlina(a){
            if (dlina < 0 ){
                dlina = 0;
                throw ("illegal parameter: dlina");
            }
            mas = new int[a];
            for (int i = 0; i < dlina; i++){
                int x = rand();
                mas[i] = x;// / RAND_MAX;
            }
        }
        Massiv(int x[],int dlinax){
            mas = new int[dlinax];
            for (int i = 0; i < dlinax; i++){
                if (x[i]<0 || x[i]>1){
                    throw ("Nepravilnyi element massiva");
                }
                mas[i] = x[i];
            }
            dlina = dlinax;
        }
        Massiv(const Massiv& x){ ////////////////////////////////
            dlina = x.dlina;
            if (dlina == 0)
                mas = NULL;
            else{
                mas = new int[dlina];
                for (int i = 0; i < dlina; i++)
                    mas[i] = x.mas[i];
            }
        }
        Massiv(const Massiv&& x){
            dlina = x.dlina;
            if (dlina == 0){
                mas = NULL;
            }
            else{
                mas = new int[dlina];
                for (int i = 0; i < dlina; i++){
                    mas[i] = x.mas[i];
                }
            }
        }
        int vivod()const{ return 0; };
        int zapolnenie(int kolvo);
        int podshchet()const{ return 0; };
        int obnovlenie();
        int zapolnenie(int kolvo, int maxdlinaper){ return 0; };
        int srednee()const;
        int dlinaq()const;
        friend std::ostream & operator<<(std::ostream &c, const Massiv &massiv){
            for (int i = 0; i < massiv.dlina; i++){
                c << massiv.mas[i] << std::endl;
            }
            return c;
        };
        Massiv & operator ++();
        Massiv operator ~();
        Massiv operator()(int, int);
        Massiv& operator =(const Massiv&);
        int operator[](int x)const;
        int&operator[](int x);
        //Circle::Circle(double a1, point a2) :aobr(a1), xy(a2){};
        ~Massiv(){
            delete(mas);
        }
    };
};
#endif /* defined(__laba2semestr3_____) */
